//
//  MoovCommunication.m
//  BLE Moov Color Test
//
//  Created by Martin Kuvandzhiev on 10/17/15.
//  Copyright © 2015 Martin Kuvandzhiev. All rights reserved.
//

#import "MoovCommunication.h"

#define DEVICE_NAME @"WhiteMoov"

NSMutableArray * deviceList;

BLECommunication * communication;

@implementation MoovCommunication
@synthesize gyroConfCharacteristic, gyroDataCharacteristic, ledColrCharacteristic, ledConfCharacteristic, ledDataCharacteristic;
- (void) initialize
{
    NSLog(@"Initializing communication");
    
    communication = [[BLECommunication alloc] init];
    
    [communication initialize];
    
    
}

- (void) searchForDevices
{
    NSLog(@"Searching for devices");
    [communication searchDevices];

}

- (void) connectToDevice
{
    NSLog(@"Connecting");
    [communication connectToDeviceWithName:DEVICE_NAME];
    double delayInSeconds = 3.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        if(communication.connected == CONNECTED)
        {
            [self getCharacteristics];
        }
        else if(communication.connected != CONNECTED)
        {
            NSLog(@"Cannot connect device");
            return;
        }
    });
    
    }

- (void) disconnect
{
    [communication disconnect];
}

- (void) getCharacteristics
{
    [communication discoverServices];
    
    double delayInSeconds = 3.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        for(CBCharacteristic * characteristic in communication.characteristicsList)
        {
            if([characteristic.UUID isEqual:[CBUUID UUIDWithString:GIRO_DATA_UUID]]) self.gyroDataCharacteristic = characteristic;
            else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:GIRO_CONF_UUID]]) self.gyroConfCharacteristic = characteristic;
            else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:LED_DATA_UUID]])
                self.ledDataCharacteristic = characteristic;
            else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:LED_COLR_UUID]])
                self.ledColrCharacteristic = characteristic;
            else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:LED_CONF_UUID]])
                self.ledConfCharacteristic = characteristic;
        }
        [self setLEDColor:0x00FF00];
        NSLog(@"Ready");
    });

}

- (NSData *) readGyroData
{
    [communication readValueForCharacteristic:self.gyroDataCharacteristic];
    
    return [communication lastReadData];
}

- (void) turnOnGyro
{
    int dataToSend = 0x01;
    [communication writeValue:[NSData dataWithBytes:&(dataToSend) length:1]forCharacteristic:self.gyroConfCharacteristic type:CBCharacteristicWriteWithResponse];
    [communication readValueForCharacteristic:self.gyroConfCharacteristic];
}

- (void) turnOffGyro
{
    int dataToSend = 0x00;
    [communication writeValue:[NSData dataWithBytes:&(dataToSend) length:1]forCharacteristic:self.gyroConfCharacteristic type:CBCharacteristicWriteWithResponse];
}

- (void) setLEDColor:(int)color
{
    NSData * data = [NSData dataWithBytes:&(color) length:3];
    [communication readValueForCharacteristic:ledDataCharacteristic];
    NSLog(@"%@", communication.lastReadData);
    [communication writeValue:data forCharacteristic:self.ledDataCharacteristic type:CBCharacteristicWriteWithResponse];
}



@end
