//
//  ViewController.h
//  BLE Moov Color Test
//
//  Created by Martin Kuvandzhiev on 10/4/15.
//  Copyright © 2015 Martin Kuvandzhiev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MoovCommunication.h"


@interface ViewController : UIViewController
- (IBAction)searchButton:(UIButton *)sender;
- (IBAction)discoverServices:(UIButton *)sender;
- (IBAction)setRGBValue:(UIButton *)sender;
- (IBAction)RSlider:(UISlider *)sender;
- (IBAction)GSlider:(UISlider *)sender;
- (IBAction)BSlider:(UISlider *)sender;
- (IBAction)disconnectButton:(UIButton *)sender;
- (IBAction)subscribeToGyro:(UISwitch *)sender;

@property char RValue;
@property char BValue;
@property char GValue;
@property int RGBValues;

@property int16_t gyroXdata;
@property int16_t gyroYdata;
@property int16_t gyroZdata;

@property (weak, nonatomic) IBOutlet UILabel *gyroX;
@property (weak, nonatomic) IBOutlet UILabel *gyroY;
@property (weak, nonatomic) IBOutlet UILabel *gyroZ;

@end


