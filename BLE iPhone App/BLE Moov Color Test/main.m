//
//  main.m
//  BLE Moov Color Test
//
//  Created by Martin Kuvandzhiev on 10/4/15.
//  Copyright © 2015 Martin Kuvandzhiev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
