//
//  ViewController.m
//  BLE Moov Color Test
//
//  Created by Martin Kuvandzhiev on 10/4/15.
//  Copyright © 2015 Martin Kuvandzhiev. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

MoovCommunication * Moov;
NSTimer * updateTimer;

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    Moov = [[MoovCommunication alloc] init];
    self.RValue = 1;
    self.GValue = 1;
    self.BValue = 1;
    updateTimer = [[NSTimer alloc] init];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)searchButton:(UIButton *)sender {
    NSLog(@"Search pressed");
    [Moov initialize];
    [NSThread sleepForTimeInterval:1.0f];
    [Moov searchForDevices];
}

- (IBAction)discoverServices:(UIButton *)sender {
    [Moov connectToDevice];
     }

- (IBAction)setRGBValue:(UIButton *)sender {
    [Moov setLEDColor:self.RGBValues];
    
}

- (IBAction)RSlider:(UISlider *)sender {
    self.RValue = sender.value;
    [self calculateRGB];
}

- (IBAction)GSlider:(UISlider *)sender {
    self.GValue = sender.value;
    [self calculateRGB];
}

- (IBAction)BSlider:(UISlider *)sender {
    self.BValue = sender.value;
    [self calculateRGB];
}

- (IBAction)disconnectButton:(UIButton *)sender {
    [Moov disconnect];
}

- (IBAction)subscribeToGyro:(UISwitch *)sender {
    if(sender.on)
    {
        [Moov turnOnGyro];
        [NSThread sleepForTimeInterval:0.5f];
        updateTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(printGyroData) userInfo:self repeats:YES];

            }
    else
    {
        [Moov turnOffGyro];
        [updateTimer invalidate];
    }
}

- (void) printGyroData
{
    // the data consist of 20 bytes
    NSData * receivedData = [Moov readGyroData];
    NSLog(@"%@", receivedData);
    //NSData * gyroXNSData = [receivedData subdataWithRange:NSMakeRange(0, 2)];
    const int8_t *bytes = [receivedData bytes];
    self.gyroXdata = (bytes[0] << 8) + bytes[1];
    self.gyroYdata = (bytes[2] << 8) + bytes[3];
    self.gyroZdata = (bytes[4] << 8) + bytes[5];
    self.gyroX.text = [NSString stringWithFormat:@"%d", self.gyroXdata];
    self.gyroY.text = [NSString stringWithFormat:@"%d", self.gyroYdata];
    self.gyroZ.text = [NSString stringWithFormat:@"%d", self.gyroZdata];
    
    
}
- (void) calculateRGB
{
    self.RGBValues = self.BValue << 16;
    self.RGBValues += self.GValue << 8;
    self.RGBValues += self.RValue;
    [Moov setLEDColor:self.RGBValues];
}



@end
