//
//  MoovCommunication.h
//  BLE Moov Color Test
//
//  Created by Martin Kuvandzhiev on 10/17/15.
//  Copyright © 2015 Martin Kuvandzhiev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BLECommunication.h"

#define DATA_UUID @"F000CD50-0451-4000-B000-000000000000"
#define GIRO_DATA_UUID @"F000CD51-0451-4000-B000-000000000000"
#define GIRO_CONF_UUID @"F000CD52-0451-4000-B000-000000000000"
#define GIRO_PERI_UUID @"F000CD53-0451-4000-B000-000000000000"
#define MOTI_COMM_UUID @"F000CD54-0451-4000-B000-000000000000"
#define MOTI_STAT_UUID @"F000CD55-0451-4000-B000-000000000000"
#define MOTI_BLOB_UUID @"F000CD56-0451-4000-B000-000000000000"
#define MAX_ACC_UUID @"F000CD60-0451-4000-B000-000000000000"
#define RD_STRT_UUID @"F000CD61-0451-4000-B000-000000000000"
#define SW_BLOB_UUID @"F000CD62-0451-4000-B000-000000000000"
#define SW_OV_UUID @"F000CD63-0451-4000-B000-000000000000"

#define BUTTON_UUID @"FFE0"
#define KEY_PRESS_STATE_UUID @"FFE1"

#define LED_UUID @"F000CD70-0451-4000-B000-000000000000"
#define LED_DATA_UUID @"F000CD71-0451-4000-B000-000000000000"
#define LED_CONF_UUID @"F000CD72-0451-4000-B000-000000000000"
#define LED_COLR_UUID @"F000CD73-0451-4000-B000-000000000000"

#define BATTERY_UUID @"2A19"


@interface MoovCommunication : NSObject
- (void) initialize;
- (void) searchForDevices;
- (void) connectToDevice;
- (void) disconnect;
- (NSData *) readGyroData;
- (void) turnOnGyro;
- (void) turnOffGyro;

- (void) setLEDEffect: (int) effect;
- (void) setLEDColor: (int) color;


- (void) readBattery;

- (void) readButton;

@property CBCharacteristic * gyroDataCharacteristic;
@property CBCharacteristic * gyroConfCharacteristic;
@property CBCharacteristic * ledDataCharacteristic;
@property CBCharacteristic * ledConfCharacteristic;
@property CBCharacteristic * ledColrCharacteristic;
@property CBCharacteristic * batteryCharacteristic;
@property CBCharacteristic * buttonCharacteristic;


@end

